import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-greeting',
  templateUrl: './greeting.component.html',
})
export class GreetingComponent implements OnInit {
  @Input() name: string;

  constructor() {
  }

  ngOnInit(): void {
  }

}

// 8. Spraw, aby pole name komponentu GreetingComponent było parametrem wejściowym. Wygoogluj "angular pass data to child component".
// Komponent app-exercises posiada pole - tablicę imion. Szablon dla każdego imienia wyświetli komponent powitalny.
