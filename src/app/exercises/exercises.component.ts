import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-exercises',
  templateUrl: './exercises.component.html'
  // template: '<div>Zadania</div>';
})
export class ExercisesComponent implements OnInit {
  poleTypuLiczbowego = 8;
  poleTypuTekstowego = 'ABCdef';
  tablicaTesktowa = ['abc', 'DEF', 'ghij', 'KLMNO'];
  tablicaImion = ['Ania', 'Kasia', 'Paweł', 'Zenon'];

  isTrue(liczba: number, tablica: string[]): boolean {
    return liczba > 7 && tablica.length > 2;
  }

  constructor() {
  }

  ngOnInit(): void {
  }

}
